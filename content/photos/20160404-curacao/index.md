+++
image = "curacao.jpg"
date = "2016-04-04"
title = "Traveling"
type = "gallery"
+++

> Once you break the walls that's been holding you captive, your journey to explore the world can finally begin.

{{< googlemaps "https://www.google.com/maps/embed?pb=!4v1606442030395!6m8!1m7!1sWxRWMJxiGSr5GMVIBGQ4ng!2m2!1d12.10262713166157!2d-68.92841233081668!3f358.41!4f-6.409999999999997!5f0.5970117501821992" >}}