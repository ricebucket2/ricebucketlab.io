+++
title = "Hello World"
date = "2020-11-25"
+++

Obligatory "Hello World"! 
<!--more-->

I am using GitLab pages to host this site. This is a static site generated using Hugo with [hugo-theme-console](https://github.com/mrmierzejewski/hugo-theme-console) theme.