+++
title = "Gambling Impulse"
date = "2020-11-25"
+++

Today was a really bad day for me caused by my uncontrollable impulse. Last few days, I've been trading systematically (sort-of) and was profitable to day dream just a little bit. I got too impatient and greedy today and broke all my rules.

<!--more-->

### Trade 1 - NIO
I was listening in on one of a very experience trader in United Trader channel around 9:25AM, right before the market opens. He said, if NIO is rejected on $50 resistance, short it, and if it breaks $50 resistance, long it. I didn't do any due diligence and as soon as the bell rang, I saw NIO dropped a few cents off $50 and immediately shorted 400 shares. Then it bounced back pass $50 and stayed there. Lost $600 in few minutes and because I didn't do my due diligence, I got emotional. I knew I shouldn't have done that.

{{< figure src="20201125-NIO.png" >}}

### Trade 2 - PLTR
I didn't recover emotionally from NIO before going into this trade. I wanted revenge.

{{< figure src="20201125-PLTR.png" >}}

```
- 09:43 -- Bought 100 shares @ $25.685 - Opened up a position. Chart looks good.
- 09:45 -- Bought 100 shares @ $26.25  - Looks like I am right. Buying more to follow trend.
- 09:48 -- Bought 100 shares @ $26.00  - Confident about this trade
- 09:50 -- Bought 100 shares @ $25.50  - Oh, shit! I was wrong again! (Emotional here). I need to average down
- 09:52 -- Bought 100 shares @ $25.20  - Oh, man! What am I doing today? I need to average down.
- 10:09 -- Bought 100 shares @ $25.05  - Ok. I guess I am bagholding this now.
- 10:14 -- Sold 200 shares @ $25.30    - Chart still looks good. Consolidation phase. Selling off my lowest average
- 10:32 -- Bought 100 shares @ $25.93  - YES! The trend is right! Bought more!
- 11:08 -- Sold 500 shares @ $25.90    - It keeps hitting this $26 resistance. Let me get out now. At least I am still green.

Total gain: $48.50 -- If I just held on and believed in my strategy, I would made all my losses back. What a bad emotional gamble!
```

### Trade 3 - CRSR
Still revenge trading. 

{{< figure src="20201125-CRSR.png" >}}

```
- 11:15 -- Bought 100 shares @ $46.35 - I looked at the chart. It looked like I missed the rally but emotionally, I still think it can continue.
11:26 -- Bought 50 shares @ $46.25  - Getting more.
- 11:38 -- Bought 50 shares @ $47.35  - Oh yea! I'm on the right trend.
- 12:01 -- Bought 25 shares @ $46.10  - Umm, okay. I didn't set a stop loss, but looks like it's holding.
- 12:07 -- Bought 25 shares @ $46.00  - I need to average down in case it pops
- 12:08 -- Bought 25 shares @ $45.80  - Oh no.
- 12:08 -- Bought 25 shares @ $45.30  - Oh no no no.
- 12:14 -- Bought 25 shares @ $44.50  - Oh no no no no.
- 12:14 -- Bought 25 shares @ $43.50  - Looks like I'm not going to recover today.

Average Buy in Price: $46.14
```
I don't know why I did that and I don't know what I'm doing. I should've sold Dec 18 $40 puts instead. I'm gonna baghold this. Worst case, I'm going to sell calls on 450 shares of CRSR. I'm pretty sure I'm gambling.
